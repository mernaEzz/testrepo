import logo from './logo.svg';
 import {BrowserRouter, Route} from 'react-router-dom';
import Report from './components/report/report';
import SliderImage from './components/report/sliderImage/sliderImage';
import MapInfo from './components/report/mapInfo/mapInfo';

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import InvPayment from './components/invPayment/invPayment';
import PaymentForInvoice from './components/paymentForInvoice/paymentForInvoice';
import InvoiceData from './components/invoiceData/invoiceData';
import SuccessPayment from './components/successPayment/successPayment';
 function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <switch>
      <Route exact path="/report" component={Report}/>
      <Route exact path="/invPayment" component={InvPayment}/>
      <Route exact path="/paymentForInvoice" component={PaymentForInvoice}/> 
      <Route exact path="/Invoice" component={InvoiceData}/>
     <Route exact path="/successPayment" component={SuccessPayment}/>
     </switch>
      </BrowserRouter>

     </div>
  );
}

export default App;
