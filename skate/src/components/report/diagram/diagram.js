import React, { Fragment } from "react";
import "./diagram.css";
import axios from "axios";
import $ from "jquery";
class Diagram extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      showKittenIndex3: null,
      showKittenIndex2: null,
      showKittenIndex4: null,
      showKittenIndex5: null,
      showKittenIndex6: null,
      show: false,
      clickedOutside: false,
      name: "w",
    };
    this.toggle1 = this.toggle1.bind(this);
    this.toggle2 = this.toggle2.bind(this);
    this.toggle3 = this.toggle3.bind(this);
    this.toggle4 = this.toggle4.bind(this);
    this.toggle5 = this.toggle5.bind(this);
    this.toggle6 = this.toggle6.bind(this);
  }
  
  componentDidMount(e) {

    axios.get("/d.json").then((resp) => {
      this.setState({ items: resp.data });
      var type=resp.data[0].tops[0].name
      console.log(type)
switch(type){
  case 'damage1':
    return(
      $('.singleDot').css("backgroundImage",'url("Dent.svg")')
     )
     break;
     case 'damage2':
       return(
        $('.singleDot').css("backgroundImage",'url("Hail.svg")')

       )
}
     });
    document.addEventListener("click", this.handleClickOutside);
    document.addEventListener("click", this.handleClickOutside2);
    document.addEventListener("click", this.handleClickOutside3);
    document.addEventListener("click", this.handleClickOutside4);
    document.addEventListener("click", this.handleClickOutside5);
 
    
  }
  componentWillUnmount() {
    document.removeEventListener("click", this.handleClickOutside);
    document.removeEventListener("click", this.handleClickOutside2);
    document.removeEventListener("click", this.handleClickOutside3);
    document.removeEventListener("click", this.handleClickOutside4);
    document.removeEventListener("click", this.handleClickOutside5);
  }
  myRef = React.createRef();
  myRef2 = React.createRef();
  myRef3 = React.createRef();
  myRef4 = React.createRef();
  myRef5 = React.createRef();
  handleClickOutside = (e) => {
    if (!this.myRef.current.contains(e.target)) {
      this.setState({
        clickedOutside: true,
        showKittenIndex2: null,
      });
    }
  };
  handleClickOutside2 = (e) => {
    if (!this.myRef2.current.contains(e.target)) {
      this.setState({
        clickedOutside: true,
        showKittenIndex3: null,
      });
    }
  };
  handleClickOutside3 = (e) => {
    if (!this.myRef3.current.contains(e.target)) {
      this.setState({
        clickedOutside: true,
        showKittenIndex4: null,
      });
    }
  };
  handleClickOutside4 = (e) => {
    if (!this.myRef4.current.contains(e.target)) {
      this.setState({
        clickedOutside: true,
        showKittenIndex5: null,
      });
    }
  };
  handleClickOutside5 = (e) => {
    if (!this.myRef5.current.contains(e.target)) {
      this.setState({
        clickedOutside: true,
        showKittenIndex6: null,
      });
    }
  };
  toggle1 = (e) => {
    this.setState({
      showKittenIndex1: e,
      showKittenIndex2: null,
      clickedOutside: false,
      showKittenIndex3: null,
    });
  };
  toggle2 = (e) => {
    this.setState({
      showKittenIndex2: e,
      showKittenIndex1: null,
      showKittenIndex3: null,
      clickedOutside: false,
    });
  };
  toggle3 = (e) => {
    this.setState({
      showKittenIndex3: e,
      showKittenIndex1: null,
      showKittenIndex2: null,
      clickedOutside: false,
    });
  };
  toggle4 = (e) => {
    this.setState({
      showKittenIndex4: e,
      showKittenIndex1: null,
      showKittenIndex2: null,
      showKittenIndex3: null,
      clickedOutside: false,
    });
  };
  toggle5 = (e) => {
    this.setState({
      showKittenIndex5: e,
      showKittenIndex1: null,
      showKittenIndex2: null,
      showKittenIndex3: null,
      showKittenIndex4: null,
      showKittenIndex6: null,

      clickedOutside: false,
    });
  };
  toggle6 = (e) => {
    this.setState({
      showKittenIndex6: e,
      showKittenIndex1: null,
      showKittenIndex2: null,
      showKittenIndex3: null,
      showKittenIndex4: null,
      showKittenIndex5: null,
      clickedOutside: false,
    });
  };
  render() {
    const items = this.state.items;
    const showKittenIndex2 = this.state.showKittenIndex2;
    const showKittenIndex3 = this.state.showKittenIndex3;
    const showKittenIndex4 = this.state.showKittenIndex4;
    const showKittenIndex5 = this.state.showKittenIndex5;
    const showKittenIndex6 = this.state.showKittenIndex6;
    const type = this.state.type;
     return (
      <div className="car-points"> 
        <div className="inspectionContainer">
          <div className="container">
            <div className="row">
              <div className="col-lg-3 col-md-5 col-sm-5 col-xs-5">
                <div>
                  <div
                    className="carContainer"
                    data-face="top"
                    
                    onClick={this.handleClickOutside}
                  >
                    {items.map((item, i) => (
                      
                      <div
                        key={i}
                         data-id={item.id}
                         ref={this.myRef}
                        name="n"
                        // {"singleDot" + " " + `${item.tops[0].name}`}
                        className="singleDot"
                        style={{
                          top: `${item.tops[0].top * 1.7 + 0.5 - 1}px`,
                          left: `${item.tops[0].left * 1.7 + 0.5 - 1}px`,
                        }}
                        onClick={() => this.toggle2(i)}
                      >
                         <div
                          className="smallPop"
                          id="d"
                          style={{
                            display: showKittenIndex2 === i ? "block" : "none",
                          }}
                        >
                          <a href={item.img} target="_blank">
                            <img src="https://cdn.syarah.com/online/inspection_points/2379/0x683/1601913002-851.jpg" />
                          </a>
                          <p>
                            <a href="c">damage1</a>{" "}
                          </p>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-5 col-sm-5 col-xs-5">
                <div className="col-md-12">
                  <div
                    className="carContainer"
                    data-face="front"
                    onClick={this.handleClickOutside}
                  >
                    {items.map((item, i) => (
                      <div
                        key={i}
                        data-id={item.id}
                        ref={this.myRef2}
                        className="singleDot"
                        style={{
                          top: `${item.fronts[0].top * 2}px`,
                          left: `${item.fronts[0].left * 2}px`,
                        }}
                        onClick={() => this.toggle3(i)}
                      >
                        <div
                          className="smallPop"
                          id="d"
                          style={{
                            display: showKittenIndex3 === i ? "block" : "none",
                          }}
                        >
                         <a href={item.img} target="_blank">
                            <img src="https://cdn.syarah.com/online/inspection_points/2379/0x683/1601913002-851.jpg" />
                          </a>
                          <p>
                            <a href="c">damage1</a>{" "}
                          </p>
                        </div>
                        <div className="pluse-container">
                          <div className="pulse-box"></div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
                <br />
                <br />
                <div className="col-md-12">
                  <div
                    className="carContainer"
                    data-face="back"
                    onClick={this.handleClickOutside}
                    onClick={this.handleClickOutside}
                  >
                    {items.map((item, i) => (
                      <div
                        key={i}
                        data-id={item.id}
                        ref={this.myRef3}
                        className="singleDot"
                        style={{
                          top: `${item.backs[0].top * 2}px`,
                          left: `${item.backs[0].left * 2}px`,
                        }}
                        onClick={() => this.toggle4(i)}
                      >
                        <div
                          className="smallPop"
                          id="d"
                          style={{
                            display: showKittenIndex4 === i ? "block" : "none",
                          }}
                        >
                         <a href={item.img} target="_blank">
                            <img src="https://cdn.syarah.com/online/inspection_points/2379/0x683/1601913002-851.jpg" />
                          </a>
                          <p>
                            <a href="c">damage1</a>{" "}
                          </p>
                        </div>
                        <div className="pluse-container">
                          <div className="pulse-box">
                            {/* <svg

                              className="pulse-svg"
                              width="12px"
                              height="12px"
                              viewBox="0 0 50 50"
                              version="1.1"
                              xmlns="http://www.w3.org/2000/svg"
                              xmlnsXlink="http://www.w3.org/1999/xlink"
                            >
                              <circle
                                className="circle first-circle"
                                fill="#0274fc"
                                cx="25"
                                cy="25"
                                r="25"
                              ></circle>
                              <circle
                                className="circle second-circle"
                                fill="#0274fc"
                                cx="25"
                                cy="25"
                                r="25"
                              ></circle>
                              <circle
                                className="circle third-circle"
                                fill="#0274fc"
                                cx="25"
                                cy="25"
                                r="25"
                              ></circle>
                              <circle
                                className="circle"
                                fill="#0274fc"
                                cx="25"
                                cy="25"
                                r="25"
                              ></circle>
                            </svg> */}
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-md-6 col-sm-6">
                <div className="col-md-12">
                  <div
                    className="carContainer"
                    data-face="left"
                    onClick={this.handleClickOutside}
                    onClick={this.handleClickOutside}
                  >
                    {items.map((item, i) => (
                      <div
                        key={i}
                        data-id={item.id}
                        ref={this.myRef4}
                        className="singleDot"
                        style={{
                          top: `${item.lefts[0].top}px`,
                          left: `${item.lefts[0].left}px`,
                        }}
                        onClick={() => this.toggle5(i)}
                      >
                        <div
                          className="smallPop"
                          id="d"
                          style={{
                            display: showKittenIndex5 === i ? "block" : "none",
                          }}
                        >
                         <a href={item.img} target="_blank">
                            <img src="https://cdn.syarah.com/online/inspection_points/2379/0x683/1601913002-851.jpg" />
                          </a>
                          <p>
                            <a href="c">damage1</a>{" "}
                          </p>
                        </div>
                        <div className="pluse-container">
                          <div className="pulse-box"></div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
                <br />
                <br />
                <div className="col-md-12">
                  <div
                    className="carContainer"
                    data-face="right"
                    onClick={this.handleClickOutside}
                    onClick={this.handleClickOutside}
                  >
                    {items.map((item, i) => (
                      <div
                        key={i}
                        data-id={item.id}
                        className="singleDot"
                        ref={this.myRef5}
                        style={{
                          top: `${item.rights[0].top}px`,
                          left: `${item.rights[0].left}px`,
                        }}
                        onClick={() => this.toggle6(i)}
                      >
                        <div
                          className="smallPop"
                          id="d"
                          style={{
                            display: showKittenIndex6 === i ? "block" : "none",
                          }}
                        >
                         <a href={item.img} target="_blank">
                            <img src="https://cdn.syarah.com/online/inspection_points/2379/0x683/1601913002-851.jpg" />
                          </a>
                          <p>
                            <a href="c">damage1</a>{" "}
                          </p>
                        </div>
                        <div className="pluse-container">
                          <div className="pulse-box"></div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Diagram;
