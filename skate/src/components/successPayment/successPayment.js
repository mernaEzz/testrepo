import React, { useEffect } from "react";
import "./successPayment.css";

const SuccessPayment = (props) => {
  useEffect(()=>{
    window.scrollTo(0,0);
    // window.addEventListener("popstate", e => {
    //   // Nope, go back to your page
    // props.history.go(1);
    // });
})
  return (
    <div className="successPayment">
      <div className="section1 text-center">
        <div className="logo"></div>
        <div className="data">
          <div className="success">
            <div className="msg">
              <i className="fa fa-check"></i>Payment successful
            </div>
          </div>
          <div className="email">
            <p>
              We've sent an invoice to <b> michaeljames@gmail.com</b>
            </p>
          </div>
          <div className="invoiceNumber">
            <p>Invoice number: 128789734</p>
          </div>
        </div>
      </div>
      <div className="section2 text-center">
        <div className="showPdf container">
          <embed
            src="3d.pdf"
            type="application/pdf"
            
          ></embed>
        </div>
      </div>
    </div>
  );
};
export default SuccessPayment;
