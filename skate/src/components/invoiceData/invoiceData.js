import React, { useEffect } from 'react';
import BillingFrom from './billingFrom/billingFrom';
import BillingInfo from './billingInfo/billingInfo';
import './invoiceData.css';
import InvoiceFooter from './invoiceFooter/invoiceFooter';
import InvoiceTable from './invoiceTable/invoiceTable';

const InvoiceData=()=>{
    useEffect(()=>{
        window.scrollTo(0,0)
    })
    return(
        <div className="invoiceData">
            <div className="container "><br/>
                <div className="invoiceData2">
                    <div className="inviceDataHeader">
                        <div className="logo"></div>
                    </div>
                    <div className="data">
                    <BillingInfo/>
                    <InvoiceTable/>
                    <BillingFrom/>
                    </div>
                    <InvoiceFooter/>
                </div>
            </div>
        </div>
    )
}
export default InvoiceData