import React from "react";
import "./invoiceTable.css";

class InvoiceTable extends React.Component {
  state = {
    items: {},
  };
  componentDidMount(){
    window.scrollTo(0,0)
  }
  render() {
    return (
      <div className="invoiceTable">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Car</th>
              <th scope="col">Services</th>
              <th scope="col" className="p">
                Price
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <div className="carData">
                  <div className="row">
                    <h5>
                      2020 MercedesBenz C300
                      <img src="Mask Group 2.svg" className="copy" />
                    </h5>
                  </div>
                  <div className="row">
                    <p>
                      AutoDeal, Inc
                      <img src="Mask Group 1.svg" className="arrow" />
                    </p>
                    <p>West Port</p>
                  </div>
                </div>
              </td>
              <td>
                <div className="services">
                  <h6>Transportation</h6>
                  <div className="row ">
                    <h6 className="">
                      Auction storager
                      <span className="has-tooltip">
                      <img src="Mask Group 5.svg" className="icon " />
                      <div className="container">
                      <span class="tooltip tooltip-left ">
                        Fees paid for the port’s services
                      </span>
                    </div>
                      </span>
                    </h6>
                   
                  </div>

                  <div className="row">
                    <h6 className="">
                      Custom
                      <span className="has-tooltip">
                      <img src="Mask Group 5.svg" className="icon2 " />
                      <div className="container">
                      <span class="tooltip tooltip-left ">
                        Fees paid for the port’s services
                      </span>
                    </div>
                      </span>
                    </h6>
                    
                  </div>
                </div>
              </td>
              <td>
                <div className="prices">
                  <h6>$150.00</h6>
                  <h6>$150.00</h6>
                  <h6>$150.00</h6>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <div className="carData">
                  <div className="row">
                    <h5>
                      2020 MercedesBenz C300
                      <img src="Mask Group 2.svg" className="copy" />
                    </h5>
                  </div>
                  <div className="row">
                    <p>
                      AutoDeal, Inc
                      <img src="Mask Group 1.svg" className="arrow" />
                    </p>
                    <p>West Port</p>
                  </div>
                </div>
              </td>
              <td>
                <div className="services">
                  <h6>Transportation</h6>
                  <div className="row ">
                    <h6 className="">
                      Auction storager
                      <span className="has-tooltip">
                      <img src="Mask Group 5.svg" className="icon " />
                      <div className="container">
                      <span class="tooltip tooltip-left ">
                        Fees paid for the port’s services
                      </span>
                    </div>
                      </span>
                    </h6>
                   
                  </div>

                  <div className="row">
                    <h6 className="">
                      Custom
                      <span className="has-tooltip">
                      <img src="Mask Group 5.svg" className="icon2 " />
                      <div className="container">
                      <span class="tooltip tooltip-left ">
                        Fees paid for the port’s services
                      </span>
                    </div>
                      </span>
                    </h6>
                    
                  </div>
                </div>
              </td>
              <td>
                <div className="prices">
                  <h6>$150.00</h6>
                  <h6>$150.00</h6>
                  <h6>$150.00</h6>
                </div>
              </td>
            </tr>

            <tr>
              <td>
                <div className="carData">
                  <div className="row">
                    <h5>
                      2020 MercedesBenz C300
                      <img src="Mask Group 2.svg" className="copy" />
                    </h5>
                  </div>
                  <div className="row">
                    <p>
                      AutoDeal, Inc
                      <img src="Mask Group 1.svg" className="arrow" />
                    </p>
                    <p>West Port</p>
                  </div>
                </div>
              </td>
              <td>
                <div className="services">
                  <h6>Transportation</h6>
                  <div className="row ">
                    <h6 className="">
                      Auction storager
                      <span className="has-tooltip">
                      <img src="Mask Group 5.svg" className="icon " />
                      <div className="container">
                      <span class="tooltip tooltip-left ">
                        Fees paid for the port’s services
                      </span>
                    </div>
                      </span>
                    </h6>
                   
                  </div>

                  <div className="row">
                    <h6 className="">
                      Custom
                      <span className="has-tooltip">
                      <img src="Mask Group 5.svg" className="icon2 " />
                      <div className="container">
                      <span class="tooltip tooltip-left ">
                        Fees paid for the port’s services
                      </span>
                    </div>
                      </span>
                    </h6>
                    
                  </div>
                </div>
              </td>
              <td>
                <div className="prices">
                  <h6>$150.00</h6>
                  <h6>$150.00</h6>
                  <h6>$150.00</h6>
                </div>
              </td>
            </tr>



            <tr>
              <td></td>
              <td>
                <div className="subtotal">
                  <h6>Subtotal:</h6>
                  <h6>Fees:</h6>
                </div>
              </td>
              <td>
                <div className="subtotal-prices">
                  <h6>$150.00</h6>
                  <h6>$150.00</h6>
                </div>
              </td>
            </tr>
            <tr>
              <td style={{ borderTop: "0" }}></td>
              <td className="total">Total:</td>
              <td className="total-price">$ 1546.51</td>
            </tr>
            <tr>
              <td style={{ borderTop: "0" }}></td>
              <td style={{ borderTop: "0" }}></td>
              <td style={{ borderTop: "0" }}>
                <div className="payBtn">
                  <button>Pay</button>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
export default InvoiceTable;
