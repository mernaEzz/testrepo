import React, { useEffect } from 'react';
import './billingInfo.css';

const BillingInfo=()=>{
    useEffect(()=>{
        window.scrollTo(0,0)
    })
    return(
        <div className="billingInfo">
           <div className="row">
               <ul>
                   <li>
                   <div className="billingTo">
                        <h5>Billing To</h5>
                        <p>Ryets Inc.</p>
                        <p>John Doe</p>
                        <p>332 South Wayside Rd,</p>
                        <p>Houston, TX</p>
                        <p>United States</p>
                    </div>
                   </li>
                   <li className="billingDataLi">
                   <div className="billing-data">
                       <div><p>Invoice number:</p><span>128734098</span></div>
                       <div><p>Invoice date:</p><span>02 Jan 2021</span></div>
                       <div><p>Due date:</p><span>07 Jan 2021</span></div>
                        
                    </div>
                   </li>
               </ul>
                   
               
                   
                    </div>
        </div>
    )
}
export default BillingInfo