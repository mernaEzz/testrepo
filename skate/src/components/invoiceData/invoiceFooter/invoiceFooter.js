import React, { useEffect } from 'react';
import './invoiceFooter.css';

const InvoiceFooter=()=>{
    useEffect(()=>{
        window.scrollTo(0,0)
    })
    return(
        <div className="invoiceFooter">
                 <p>Copyright © 2021 Ryets. All rights reserved.</p>
         </div>
    )
}
export default InvoiceFooter